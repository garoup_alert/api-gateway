FROM maven:3-jdk-8-alpine

ENV TZ=Asia/Seoul

WORKDIR /usr/src/app

COPY ./api-gateway-boot/target/api-gateway-boot-1.0-spring-boot.jar /usr/src/app

EXPOSE 9000

CMD java -jar api-gateway-boot-1.0-spring-boot.jar
